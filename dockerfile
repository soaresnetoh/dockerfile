FROM ubuntu

LABEL MANTEINER="Hernani Soares <soaresnetoh@gmail.com>"
LABEL APP_VERSION="1.0.0"

ENV NPM_VERSION=8 ENVIROMENT=PROD

RUN apt-get update && apt-get install -y git nano npm

WORKDIR /usr/share/myapp

RUN npm build

COPY requirements.txt requirements.txt

ADD file.tar.gz ./

RUN useradd joao

USER joao

EXPOSE 8080

VOLUME [ "/data" ]

ENTRYPOINT [ "ping" ]

CMD [ "localhost" ]